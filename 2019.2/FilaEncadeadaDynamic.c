#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

// Estrutura de Fila Encadeada.
typedef struct no {
	struct no * proximo;
	int valor;
} node;

node * newNo (int v) {
	node * no = malloc(sizeof(node));
	no->proximo = NULL;
	no->valor = v;
}

typedef struct structFila { 
	int tamanho;
	node * inicio; 
	node * final;
} Fila;

// Cria uma Fila
Fila * criar () { 
	
	Fila * p = malloc(sizeof(Fila)); 
	
	if(p) {
		p->inicio = NULL;
		p->final = NULL;
		p->tamanho = 0;
	}
	
	return p;
} 

// Isere um elemento na Fila
void inserir(Fila * p, int A){
	
	if(p) {
		
		node * no = newNo(A);
		
		if (p->tamanho > 0) {
			p->final->proximo = no;
			p->final = no;
		}
		else {
			p->inicio = no;
			p->final = no;
		}
		
		p->tamanho++;
	}
} 

// Remover um elemento da Fila
int remover(Fila * p) {
	int r = 0;
	
	if(p && p->tamanho > 0) {
		
		r = p->inicio->valor;
		node * aux = p->inicio;
		
		p->inicio = p->inicio->proximo;
		free(aux);
		p->tamanho--;
		
		if (p->tamanho <= 0) {
			p->inicio = NULL;
			p->final = NULL;
		}
	}
	
	return r;
}

// Retorna o tamanho da Fila
int tamanho(Fila * p) { 
	
	if(p) {
		return p->tamanho;
	}
	
	return 0;
} 

int recuperaInicio(Fila * p) {
	
	if(p && p->tamanho > 0) {
		return p->inicio->valor;
	}
	
	return 0;
}

int recuperaFinal(Fila * p) {
	
	if(p && p->tamanho > 0) {
		return p->final->valor;
	}
	
	return 0;
}

// Destroi uma Fila
void destruir(Fila * p) { 

	if(p && p->tamanho > 0) {
		
		while (tamanho(p)) {
			remover(p);
		}
		
		free(p);
		p = NULL;
	}
}

void exibirFila (Fila * p) {
	
	printf("{");
	
	if(p && p->tamanho > 0) {
		
		int i = 0;
		node * aux = p->inicio;
		
		for (i = 0; i < p->tamanho; i++) {
			if (i < p->tamanho - 1) {
				printf(" %i,", aux->valor);
			}
			else {
				printf(" %i ", aux->valor);
			}
			aux = aux->proximo;
		}
	}
	
	printf("}\n");
}

int main()
{
	
	Fila * queue = criar();
	
	inserir(queue, 45);
	exibirFila(queue);
	
	inserir(queue, 71);
	exibirFila(queue);
	
	inserir(queue, 100);
	exibirFila(queue);
	
	remover(queue);
	exibirFila(queue);
	
	remover(queue);
	exibirFila(queue);
	
	remover(queue);
	exibirFila(queue);
	
	remover(queue);
	exibirFila(queue);
	destruir(queue);
	
	return 0;
}
