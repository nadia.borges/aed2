#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

// Estrutura de Pilha Indexada Dinamicamente.
typedef struct structPilha { 
	int * elementos; 
	int topo; 
} Pilha;

// Cria uma pilha
Pilha * criar (int n) { 
	
	Pilha * p = malloc(sizeof(Pilha)); 
	
	if(p) {
		p->topo = 0;
		p->elementos = malloc(sizeof(int) * n);
	}
	
	return p;
} 

// Empilha um elemento; conhecido como metodo Push da Pilha
void empilha(Pilha * p, int A){ 
	if(p) {
		p->elementos[p->topo] = A; 
		p->topo++;
	}
} 

// Desemplilha um elemento; conhecido como metodo Pop da Pilha
int desempilha(Pilha * p) {
	if(p) {
		p->topo--;
		return p->elementos[p->topo]; 
	}
	return 0;
}

// Retorna o tamanho da pilha
int tamanho(Pilha * p) { 
	if(p) {
		return p->topo;
	}
	
	return 0;
} 

// Destroi uma pilha
void destruir(Pilha * p) { 
	if(p) {
		free(p);
		p = NULL;
	}
}

void exibirPilha (Pilha * p) {
	
	if(p) {
		int i = 0;
		printf("{");
		for (i = p->topo - 1; i >= 0; i--) {
			if (i > 0) {
				printf(" %i,", p->elementos[i]);
			}
			else {
				printf(" %i ", p->elementos[i]);
			}
		}
		printf("}\n");
	}
}

int main()
{
	Pilha * stack = criar(100);
	int passo = 1;
	
	printf("Passo %2i: ", passo++);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 5);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 6);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 8);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 78);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 51);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %2i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("Passo %2i: ", passo++);
	empilha(stack, 12);
	exibirPilha(stack);
	
	printf("\nTamanho: %i\n\n", tamanho(stack));
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	printf("[DESEMPILHA]\n");
	printf("Passo %i: ", passo++);
	desempilha(stack);
	exibirPilha(stack);
	
	destruir(stack);
	
	return 0;
}
