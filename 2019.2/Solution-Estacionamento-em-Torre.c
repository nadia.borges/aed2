#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

// Estrutura de Pilha Encadeada.
typedef struct no {
	struct no * proximo;
	int valor;
} node;

node * newNo (int v) {
	node * no = malloc(sizeof(node));
	no->proximo = NULL;
	no->valor = v;
}

typedef struct structPilha { 
	int tamanho; 
	node * topo; 
} Pilha;

// Cria uma pilha
Pilha * criar () { 
	
	Pilha * p = malloc(sizeof(Pilha)); 
	
	if(p) {
		p->topo = NULL;
		p->tamanho = 0;
	}
	
	return p;
} 

// Empilha um elemento; conhecido como metodo Push da Pilha
void empilha(Pilha * p, int A){
	
	if(p) {
		
		node * no = newNo(A);
		
		no->proximo = p->topo;
		p->topo = no;
		p->tamanho++;
	}
} 

int recuperaTopo (Pilha * p) {
	
	if(p && p->tamanho > 0) {
		return p->topo->valor;
	}
	
	return 0;
}


// Desemplilha um elemento; conhecido como metodo Pop da Pilha
int desempilha(Pilha * p) {
	
	if(p && p->tamanho > 0) {
		
		int r = p->topo->valor;
		
		node * aux = p->topo;
		p->topo = p->topo->proximo;
		free(aux);
		p->tamanho--;
		
		return r; 
	}
	
	return 0;
}

// Retorna o tamanho da pilha
int tamanho(Pilha * p) { 
	
	if(p) {
		return p->tamanho;
	}
	
	return 0;
} 

// Destroi uma pilha
void destruir(Pilha * p) { 

	if(p && p->tamanho > 0) {
		
		while (tamanho(p)) {
			desempilha(p);
		}
		
		free(p);
		p = NULL;
	}
}

void exibirPilha (Pilha * p) {
	
	if(p && p->tamanho > 0) {
		
		int i = 0;
		node * aux = p->topo;
		
		printf("{");
		for (i = 0; i < p->tamanho; i++) {
			if (i < p->tamanho - 1) {
				printf(" %i,", aux->valor);
			}
			else {
				printf(" %i ", aux->valor);
			}
			aux = aux->proximo;
		}
		printf("}\n");
	}
}

int main()
{
	Pilha * stack = criar();
	
	int nCarros = 0, tamGarage = 0, placa = 0;
	
	scanf("%i %i", &nCarros, &tamGarage);
	
	int i = 0;
	
	for (i = 0; i < nCarros; i++) {
		
		scanf("%i", &placa);
		
		if (tamanho(stack) < tamGarage) {
			empilha(stack, placa);
		}
		else {
			printf("Cheio\n");
		}
	}
	
	int nRetirar = 0;
	int t = 0;
	int size = 0;
	
	do {
		
		scanf("%i", &nRetirar);
		
		for (i = 0; i < nRetirar; i++) {
			desempilha(stack);
		}
		
		if (nRetirar > 0) {	
			t = recuperaTopo(stack);
			size = tamanho(stack);
			printf("%i %i\n", t, size);
		}
		
	} while (nRetirar > 0);
	
	destruir(stack);
	
	return 0;
}
