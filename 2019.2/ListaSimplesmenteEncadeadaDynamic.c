#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

// Estrutura de Lista Simplesmente Encadeada.
typedef struct sNo No;

struct sNo {
	No * proximo;
	int valor;
};

typedef struct structLista { 
	int tamanho;
	No * inicio; 
	No * final;
} Lista;

No * newNo (int v) {
	No * node = malloc(sizeof(No));
	node->proximo = NULL;
	node->valor = v;
}

// Cria uma Lista
Lista * criar () { 
	
	Lista * p = malloc(sizeof(Lista)); 
	
	if(p) {
		p->inicio = NULL;
		p->final = NULL;
		p->tamanho = 0;
	}
	
	return p;
}

No * recuperaNo (Lista * p, int k) {
	
	int i = 0;
	
	No * n = p->inicio;
	
	for (i = 0; i < k; i++) {
		n = n->proximo;
	}
	
	return n;
}


// Isere um elemento no Inicio da Lista
void inserirInicio(Lista * p, int A){
	
	if(p) {
		
		No * node = newNo(A);
		
		if (p->tamanho > 0) {
			node->proximo = p->inicio;
			p->inicio = node;
		}
		else {
			p->inicio = node;
			p->final = node;
		}
		
		p->tamanho++;
	}
}

// Isere um elemento no Final da Lista
void inserirFinal(Lista * p, int A){
	
	if(p) {
		
		No * node = newNo(A);
		
		if (p->tamanho > 0) {
			p->final->proximo = node;
			p->final = node;
		}
		else {
			p->inicio = node;
			p->final = node;
		}
		
		p->tamanho++;
	}
}

// Remover um elemento do Inicio da Lista
int removerInicio(Lista * p) {
	int r = 0;
	
	if(p && p->tamanho > 0) {
		
		r = p->inicio->valor;
		No * aux = p->inicio;
		p->inicio = p->inicio->proximo;
		
		free(aux);
		p->tamanho--;
		
		if (!p->tamanho) {
			p->inicio = NULL;
			p->final = NULL;
		}
	}
	
	return r;
}

// Remover um elemento do Final da Lista
int removerFinal(Lista * p) {
	int r = 0;
	
	if(p && p->tamanho > 0) {
		
		r = p->final->valor;
		No * aux = p->final;
		
		free(aux);
		p->tamanho--;
		
		p->final = recuperaNo(p, p->tamanho - 1);
		p->final->proximo = NULL;

		if (p->tamanho == 1) {
			p->final = p->inicio;	
		}
		else if (p->tamanho == 0) {
			p->inicio = NULL;
			p->final = NULL;
		}
	}
	
	return r;
}

void inserirPosicaoK(Lista * p, int A, int k){
	if(p) {
		
		if(p->tamanho == 0) {
			inserirFinal(p, A);
		}
		else if (p->tamanho == 1) {
			
			if (p->tamanho == k) {
				inserirFinal(p, A);
			}
			else {
				inserirInicio(p, A);
			}
		}
		else {
			
			if (k == 0) {
				inserirInicio(p, A);
			}
			else if (k == p->tamanho) {
				inserirFinal(p, A);
			}
			else {
				No * node = newNo(A);
				No * aux1 = recuperaNo(p, k - 1);
				
				node->proximo = aux1->proximo;
				aux1->proximo = node;
				p->tamanho++;
			}
		}
	}
}

int removerPosicaoK(Lista * p, int k) {
	int r = 0;
	
	if (k < 0) {
		return 0;
	}
	
	if(p && p->tamanho > 0) {
		if (k == 0) {
			r = removerInicio(p);
		}
		else if (p->tamanho - 1 == k) {
			r = removerFinal(p);
		}
		else {
			
			No * aux1 = recuperaNo(p, k - 1);
			No * aux2 = aux1->proximo;
			
			aux1->proximo = aux2->proximo;
			
			r = aux2->valor;
			
			free(aux2);
			p->tamanho--;
		}
	}
	
	return r;
}


// Retorna o tamanho da Lista
int tamanho(Lista * p) { 
	
	if(p) {
		return p->tamanho;
	}
	
	return 0;
} 

int recuperaInicio(Lista * p) {
	
	if(p && p->tamanho > 0) {
		return p->inicio->valor;
	}
	
	return 0;
}

int recuperaFinal(Lista * p) {
	
	if(p && p->tamanho > 0) {
		return p->final->valor;
	}
	
	return 0;
}

void exibirLista (Lista * p) {
	
	printf("{");
	
	if(p && p->tamanho > 0) {
		
		int i = 0;
		No * aux = p->inicio;
		
		for (i = 0; i < p->tamanho; i++) {
			if (i < p->tamanho - 1) {
				printf(" %i,", aux->valor);
			}
			else {
				printf(" %i ", aux->valor);
			}
			aux = aux->proximo;
		}
	}
	
	printf("}\n");
}

// Destroi uma Lista
void destruir(Lista * p) { 

	if(p && p->tamanho > 0) {
		
		while (tamanho(p)) {
			removerInicio(p);
		}
		
		free(p);
		p = NULL;
	}
}

int main()
{
	int i = 0;
	Lista * L = criar();
	
	exibirLista(L);
	
	for (i = 0; i < 4; i++) {
		inserirFinal(L, i + 1);
		exibirLista(L);
	}
	
	inserirPosicaoK(L, 88, 3);
	exibirLista(L);
	
	inserirPosicaoK(L, 99, 2);
	exibirLista(L);
	
	inserirPosicaoK(L, 55, 6);
	exibirLista(L);

	inserirPosicaoK(L, 44, 0);
	exibirLista(L);
	
	destruir(L);
	
	return 0;
}
