#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

typedef struct {
	char nome[100];
	int numero_conta;
} Cliente;

// Estrutura de Fila Encadeada.
typedef struct no {
	struct no * proximo;
	Cliente valor;
} node;

node * newNo (Cliente v) {
	node * no = malloc(sizeof(node));
	no->proximo = NULL;
	no->valor = v;
}

typedef struct structFila { 
	int tamanho;
	node * inicio; 
	node * final;
} Fila;

// Cria uma Fila
Fila * criar () { 
	
	Fila * p = malloc(sizeof(Fila)); 
	
	if(p) {
		p->inicio = NULL;
		p->final = NULL;
		p->tamanho = 0;
	}
	
	return p;
} 

// Isere um elemento na Fila
void inserir(Fila * p, Cliente A){
	
	if(p) {
		
		node * no = newNo(A);
		
		if (p->tamanho > 0) {
			p->final->proximo = no;
			p->final = no;
		}
		else {
			p->inicio = no;
			p->final = no;
		}
		
		p->tamanho++;
	}
} 

// Remover um elemento da Fila
Cliente remover(Fila * p) {
	Cliente r;
	
	if(p && p->tamanho > 0) {
		
		r = p->inicio->valor;
		node * aux = p->inicio;
		
		p->inicio = p->inicio->proximo;
		free(aux);
		p->tamanho--;
		
		if (!p->tamanho) {
			p->final = NULL;
		}
	}
	
	return r;
}

// Retorna o tamanho da Fila
int tamanho(Fila * p) { 
	
	if(p) {
		return p->tamanho;
	}
	
	return 0;
} 

Cliente recuperaInicio(Fila * p) {
	
	if(p && p->tamanho > 0) {
		return p->inicio->valor;
	}
	
	return;
}

Cliente recuperaFinal(Fila * p) {
	
	if(p && p->tamanho > 0) {
		return p->final->valor;
	}
	
	return;
}

// Destroi uma Fila
void destruir(Fila * p) { 

	if(p && p->tamanho > 0) {
		
		while (tamanho(p)) {
			remover(p);
		}
		
		free(p);
		p = NULL;
	}
}

void exibirFila (Fila * p) {
	
	printf("{");
	
	if(p && p->tamanho > 0) {
		
		int i = 0;
		node * aux = p->inicio;
		
		for (i = 0; i < p->tamanho; i++) {
			if (i < p->tamanho - 1) {
				printf(" %i,", aux->valor.numero_conta);
			}
			else {
				printf(" %i ", aux->valor.numero_conta);
			}
			aux = aux->proximo;
		}
	}
	
	printf("}\n");
}

Cliente desenfileirar (Fila * f) { 
	return remover(f);
}

void enfileirar (Fila * f, int numero_conta, char * nome) {  
	Cliente c;
	strcpy(c.nome, nome);
	c.numero_conta = numero_conta;
	inserir(f, c);
}

int main() 
{

	Fila * fila = criar();
    
    int numero_conta = 0;
	char * nome;

	do {
		
		scanf("%i", &numero_conta);

		if (numero_conta > 0) {

			nome = malloc(sizeof(char) * 100);
			scanf("%s", nome);

			enfileirar(fila, numero_conta, nome);
		}

	} while (numero_conta > 0);
	
	Cliente c;

	do { 
		
		c = desenfileirar(fila);
		printf("CONTA: %i | CLIENTE: %s\n", c.numero_conta, c.nome);

	} while (fila->tamanho > 0);

	return 0;
}
