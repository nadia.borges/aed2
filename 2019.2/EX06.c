#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

int EhDivisaoExata(int dividendo, int divisor) {
    return (dividendo % divisor) == 0;
}

int EhPrimo(int num) {
	
    int i, quantDivisores = 0;

    for(i = 1; i <= num; i++) {
        if (EhDivisaoExata(num, i)) {
            quantDivisores++;
        }
    }

    if (quantDivisores == 2) {
        return 1;
    } else {
        return 0;
    }
}

int main () { 

	char nomeArquivo[100];
	
	int inicio = 0, final  = 0, i = 0;
	
	scanf("%i %i %s", &inicio, &final, &nomeArquivo);
	
	FILE * f = fopen (nomeArquivo, "wt");
	
	for (i = inicio; i <= final; i++) {
		
		if (!EhPrimo(i)) {
			fprintf(f, "%i ", i);
		}
	}
	
	printf("OK.");
	
 	fclose (f);
 	
	return 0;
}
