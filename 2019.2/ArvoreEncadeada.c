#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

typedef struct sNo No;

struct sNo {
	No * esq;
	No * dir;
	int val;
};

typedef struct { 
	int tam;
	No * raiz;
} ArvBin;

No * newNo (int v){
	
	No * no = malloc(sizeof(No));
	
	no->esq = NULL;
	no->dir = NULL;
	no->val = v;
	
	return no;
}

ArvBin * newArvBin () {
	ArvBin * tree = malloc(sizeof(ArvBin));
	
	tree->tam = 0;
	tree->raiz = NULL;
	
	return tree;
}

No * buscaAnterior (No * no, int v) {
	
	No * ant = NULL;
	
	while (no != NULL && no->val != v) {
		
		ant = no;
		
		if (no->val < v) {
			no = no->dir;
		}
		else if(no->val > v) {
			no = no->esq;
		}
		else {
			return NULL;
		}
	}
	
	return ant;
}

No * buscaNo (No * no, int v) {
	
	while (no != NULL) {
		
		if (no->val == v) {
			return no;
		}
		
		if (no->val < v) {
			no = no->dir;
		}
		else if(no->val > v) {
			no = no->esq;
		}
	}
	
	return NULL;
}

void inserirNo(ArvBin * tree, int v) {
	
	if (tree->tam > 0) {
		
		No * ant = buscaAnterior(tree->raiz, v);
		
		if (ant) {
			
			if( ant->val > v) {
				ant->esq = newNo(v);
			}
			else {
				ant->dir = newNo(v);
			}
			tree->tam++;
		}
	}
	else {
		tree->raiz = newNo(v);
		tree->tam++;
	}
}

void removerNo(ArvBin * tree, int v) {
	
	No * parent = NULL;
	No * no = tree->raiz;
	No * e = NULL;
	No * mesq = NULL;
	No * mdir = NULL;
	No * ant = NULL;
	
	if (tree->tam > 1) {
		
		while (no != NULL) {
			
			if (no->val == v) {
				
				if(no->dir) {
					
					for(e = no->dir; e != NULL; e = e->esq) {
						ant = mesq;
						mesq = e;
					}
					mesq->esq = no->esq;
					
					ant->esq = NULL;
					
					for(e = mesq->dir; e != NULL; e = e->dir) {
						mdir = e;
					}
					mdir->dir = no->dir;
										
					if (parent){
						parent->esq = mesq;
					}
				}
				else {
					if (parent){
						parent->esq = no->esq;
					}
				}
				
				free(no);
				tree->tam--;
				
				return;
			}
			
			parent = no;
			
			if (no->val < v) {
				no = no->dir;
			}
			else if(no->val > v) {
				no = no->esq;
			}
		}
	}
	else if (tree->tam == 1){
		free(tree->raiz);
		tree->raiz = NULL;
		tree->tam--;
	}
}

int alturaArvore(ArvBin * tree) {
	
}

int quantidadeNos(ArvBin * tree) {
	return tree->tam;
}

void destruirArvore(ArvBin * tree) {
	free(tree);
}

int main () {
	
	
	ArvBin * a = newArvBin();
	
	inserirNo(a, 5);
	inserirNo(a, 3);
	inserirNo(a, 60);
	inserirNo(a, 34);
	inserirNo(a, 61);
	inserirNo(a, 9);
	inserirNo(a, 8);
	inserirNo(a, 51);
	inserirNo(a, 40);
	inserirNo(a, 52);
	inserirNo(a, 35);
	inserirNo(a, 42);
	inserirNo(a, 41);
	inserirNo(a, 43);
	inserirNo(a, 36);
	inserirNo(a, 37);
	inserirNo(a, 38);
	
	No * n = NULL;
	No * ant = NULL;
	
	n = buscaNo(a->raiz, 34);
	ant = buscaAnterior(a->raiz, n->val);
	printf("\n %i --> %i --> (%i, %i)", (ant) ? ant->val : -1, n->val, (n->esq) ? n->esq->val: -1, (n->dir) ? n->dir->val : -1);
	
	removerNo(a, 34);
	printf("\n REMOVEU O %i", 34);
	
	n = buscaNo(a->raiz, 34);
	if (n) {
		printf("\n NAO ACHOU %i", 34);
	}
	
	n = buscaNo(a->raiz, 35);
	ant = buscaAnterior(a->raiz, n->val);
	printf("\n %i --> %i --> (%i, %i)", (ant) ? ant->val : -1, n->val, (n->esq) ? n->esq->val: -1, (n->dir) ? n->dir->val : -1);
	
	n = buscaNo(a->raiz, 38);
	ant = buscaAnterior(a->raiz, n->val);
	printf("\n %i --> %i --> (%i, %i)", (ant) ? ant->val : -1, n->val, (n->esq) ? n->esq->val: -1, (n->dir) ? n->dir->val : -1);
	
	n = buscaNo(a->raiz, 40);
	ant = buscaAnterior(a->raiz, n->val);
	printf("\n %i --> %i --> (%i, %i)", (ant) ? ant->val : -1, n->val, (n->esq) ? n->esq->val: -1, (n->dir) ? n->dir->val : -1);
	
	destruirArvore(a);
	
	return 0;
}









