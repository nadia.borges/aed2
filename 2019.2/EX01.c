#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

/*
	Receber N numeros e exibir de forma ordenada.
*/

void ordena (int * numeros, int n) {
	
	int i = 0, j = 0;
	int aux = 0;
	
	for (i = 0; i < n - 1; i++)
    {
		for (j = i + 1; j < n; j++)
	    {
			if (numeros[i] > numeros[j]) {
				aux = numeros[i];
				numeros[i] = numeros[j];
				numeros[j] = aux;
			}
		}
	}
}

int main () {
	
	int numero[100], count = 0, i = 0;
	 
	scanf("%i", &count);
	
	for (i = 0; i < count; i++)
	{
	    scanf("%i", &numero[i]);
	}
	
	ordena (numero, count);
	
	for (i = 0; i < count; i++)
	{
	    printf("%i, ", numero[i]);
	}
	
	return 0;
}
