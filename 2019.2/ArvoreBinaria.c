#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

typedef struct sNo No;

struct sNo {
    No * esq;
    int valor;
	No * dir;
};

typedef No * ArvBin;

ArvBin * createTree () {
    ArvBin * raiz = malloc(sizeof(ArvBin));
    *raiz = NULL;
    return raiz;
}

No * newNO (int v) {

    No * no = malloc(sizeof(No));

    no->esq = NULL;
    no->dir = NULL;
    no->valor = 0;

    return no;
}

int height (ArvBin * tree) {
    
    int t = 1;

    if (!tree) {
        return 0;
    }

    if (!(*tree)) {
        return 0;
    }

    t += height( (*tree)->esq);
    t += height( (*tree)->dir);

    return t;
}

int main () {

    ArvBin * tree = createTree();
    *tree = newNO(5);

    int h = height(tree);

    printf("teste %i", h);
}