#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

int main () {

	FILE * f = fopen ("compra.txt", "rt");

	char produto[100];
	int quantidade = 0;
	float valor = 0.0, total = 0.0;

	while (fscanf(f, "%s %i %f", &produto, &quantidade, &valor) != EOF) {
		printf("PRODUTO: %s QUANTIDADE: %i VR-UNITARIO: %.2f\n", produto, quantidade, valor);
		total = total + (valor * quantidade);
	}
	
	printf("TOTAL DA COMPRA: %.2f", total);
	
 	fclose (f);
 	
	return 0;
}

