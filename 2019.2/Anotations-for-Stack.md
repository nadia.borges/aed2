Operaçãoes necessárias para que uma estrutura seja considerada uma Pilha (Stack).

* Criar uma pilha. 			 (Default)
* Recuperar o tamanho da pilha. (Default)
* Destruir uma pilha. 			 (Default)

* Empilhar um elemento. 		 (Push)
* Desempilhar um elemento. 	 (Pop)



