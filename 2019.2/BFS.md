
#### BFS - Breadth First Search

- **Description**:
    - **WIDTH** search algorithm in a graph.
    - A search algorithm is an algorithm that traverses a graph by walking the arcs from one vertex to another. 
    - After visiting the starting end of an arc, the algorithm traverses the arc and visits its ending end. 
    - Each arc is traversed at most once.

+ **Characterics Graph**
    + Edges UnWeighted

+ **Complexness**
    $ O\left( V + E \right) $
    
+ **Group Name**
    + Single Source Shortest Path

```c
BFS(G, s, t):
   
    c = [];
    d = []
    fila = []
    visited = [];
   
    for each u of G.V do:
        visited[u] = 0
   
    fila.Enqueue(G.V[s]);
    visited[s] = 1
   
    while not fila.IsEmpty() do:
       
        u = fila.Dequeue(0);
       
        for each v of u.Adjacent() do:
           
            if (visited[v] == 0) then:   
            
                visited[v] = 1
                fila.Enqueue(v)
               
                c[v] = u
               
                if d[v] > d[u] + 1 then:
                    d[v] = d[u] + 1
                   
                if (v == t) then:
                    return c
```