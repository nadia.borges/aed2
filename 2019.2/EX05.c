#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

// Estrutura definida pelo Programador (Struct)
typedef struct {
	char nome[100];
	int peso;
	float altura;
	int idade;
	int sexo;
} pessoa;

int main () {
   
   pessoa * p1 = malloc(sizeof(pessoa) * 1); // alocacao dinamica para o ponteiro p1
   pessoa * p2 = malloc(sizeof(pessoa) * 1); // alocacao dinamica para o ponteiro p1
   
   pessoa * p3 = p2; // ponteiro p3 aponta para o mesmo lugar de p2.
   
   p3 = p1; // p3 aponta para o mesmo lugar de p1.
   p1 = p2; // p1 aponta para o mesmo lugar de p2.
   
   p1->idade = 11;
   p2->idade = 12;
   p3->idade = 14;
   
   printf("%i", p1->idade);
   
   return 0;
}
