#include "stdio.h"
#include "stdlib.h"
#include "math.h"
#include "string.h"

int main () {

	FILE * f = fopen ("notas.txt", "rt");

	char aluno[100];
	float nota = 0.0, total = 0.0, maior = 0.0;
	int quantidade = 0;
	
	while (fscanf(f, "NOME: %s NOTA: %f\n", &aluno, &nota) != EOF) {
		
		maior = (maior < nota) ? nota : maior;
		
		total += nota;
		quantidade++;
	}
	
	float media = total / quantidade;
	
	printf("MEDIA: %.2f\n", media);
	printf("MAIOR: %.2f", maior);
	
 	fclose (f);
 	
	return 0;
}

