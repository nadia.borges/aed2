#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

typedef struct sNo No;

struct sNo {

	No * proximo;
	int valor;
};

typedef struct {

	No * inicio;
	No * final;

	int tamanho;

} Lista;

No * novoNo (int valor) {
	No * a = malloc(sizeof(No));
	a->valor = valor;
	a->proximo = NULL;

	return a;
}

void inserir (Lista * l, int valor, int x) {

	No * e = NULL;
	No * ant = NULL;

	No * a = novoNo(valor);

    if (x >= 0 && x <= l->tamanho) {

        int i = 0;
        for (e = l->inicio, ant = e; e != NULL; ant = e, e = e->proximo, i++) {

            if (i == x - 1 || x == 0) {

                if (ant == e && x == 0) {

                    l->inicio = a;
                    a->proximo = e;
                    l->tamanho++;

                    return;
                }
                else {
                    if (e->proximo) {

                        a->proximo = e->proximo;
                        e->proximo = a;
                        l->tamanho++;

                        return;
                    }
                    else {

                        e->proximo = a;
						l->final = a;
                        l->tamanho++;

                        return;
                    }
                }
            }
        }

        if (l->inicio == NULL) {
            l->inicio = a;
			l->final = a;
            l->tamanho++;
        }
    }
    else {
        printf("Posicao %i esta fora da lista.\n", x);
    }
}

int remover (Lista * l, int x) {

    int valor = 0;

	No * e = NULL;
	No * ant = NULL;
    No * lixo = NULL;

    if (l->inicio == NULL) {
		printf("Lista esta vazia.\n");
	}
    else if (x >= 0 && x < l->tamanho) {

        int i = 0;
        for (e = l->inicio, ant = e; e != NULL; ant = e, e = e->proximo, i++) {

            if (i == x || x == 0) {

                if (ant == e && x == 0) {

                    lixo = l->inicio;

                    l->inicio = l->inicio->proximo;
					l->final = l->inicio->proximo;
					
                    valor = lixo->valor;

                    free(lixo);
                    l->tamanho--;

                    return valor;
                }
                else {
                    if (e->proximo) {

                        ant->proximo = e->proximo;
                        valor = e->valor;
                        free(e);
                        l->tamanho--;

                        return valor;
                    }
                    else {

                        ant->proximo = NULL;
						l->final = ant;
						
                        valor = e->valor;

                        free(e);
                        l->tamanho--;

                        return valor;
                    }
                }
            }
        }
    }
    else {
        printf("Posicao %i esta fora da lista.\n", x);
    }

	return valor;
}

int recuperar (Lista * l, int x) {

    int valor = 0;

	No * e = NULL;
	
    if (l->inicio == NULL) {
		printf("Lista esta vazia.\n");
	}
    else if (x >= 0 && x < l->tamanho) {

        int i = 0;
        for (e = l->inicio; e != NULL; e = e->proximo, i++) {

            if (i == x) {
                return e->valor;
            }
        }
    }
    else {
        printf("Posicao %i esta fora da lista.\n", x);
    }

	return valor;
}

void inserir_inicio (Lista * l, int valor) {
    inserir(l, valor, 0);
}

int remover_inicio (Lista * l) {
    return remover(l, 0);
}

void inserir_final (Lista * l, int valor) {
    inserir(l, valor, l->tamanho);
}

int remover_final (Lista * l) {
	return remover(l, l->tamanho - 1);
}

void exibir_lista (Lista * l) {

	No * e = NULL;

	int i = 0;
	
	printf("--------------\nTAMANHO: %i\n--------------\n", l->tamanho);

	printf("|");

	for(i = 0; i < l->tamanho; i++)
	{
		printf(" %2.2i |", i);
	}
	
	printf("\n");
	printf("|");

	for(i = 0; i < l->tamanho; i++)
	{
		printf("----|");
	}
	
	printf("\n");
	printf("|");

	for (e = l->inicio; e != NULL; e = e->proximo) {
		printf(" %2i |", e->valor);
	}
    printf("\n\n");
}

void exibir_inicio_lista (Lista * l) {
	printf("----------\n");
	printf("INICIO: %i\n", l->inicio->valor);
	printf("----------\n\n");
}

void exibir_final_lista (Lista * l) {
	printf("----------\n");
	printf("FINAL: %3i\n", l->final->valor);
	printf("----------\n\n");
}

Lista * criar() {

	Lista * l = malloc(sizeof(Lista));

	l->tamanho = 0;
	l->inicio = NULL;
	l->final = NULL;

    FILE * arq = fopen("numeros.txt", "rt");
    int numero = 0;
    while (fscanf(arq, "%i ", &numero) != EOF) {
        inserir_final(l, numero);
    }

    fclose(arq);

	return l;
}

int main() 
{

	Lista * lista = criar();
    
    // remover N� 2;
	remover(lista, 2);

    // adicionar N� com valor 75 no final da lista;
    inserir_final(lista, 75);

    // adicionar N� com valor 53 no inicio da lista;
    inserir_inicio(lista, 53);

    // adicionar N� com valor 43 na posi��o 0 (zero);
    inserir(lista, 43, 0);

    // adicionar N� com valor 36 na posi��o 2 (zero);
    inserir(lista, 36, 2);

    // remover N� final;
    remover_final(lista);

    // exibir o N� inicial;
    exibir_inicio_lista(lista);

    // exibir o N� final;
    exibir_final_lista(lista);

    // exibir toda lista;
    exibir_lista(lista);

    // recuperar o N� da posi��o 3;
    printf("Lista[%i] => %i", 3, recuperar(lista, 3));

	return 0;
}

